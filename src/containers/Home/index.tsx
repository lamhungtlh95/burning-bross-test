import { useEffect, useState, useRef, useCallback } from "react";
import { getProducts, getProductsByName } from "../../api/product";
import product from "../../modal/product";
import "./index.css";
import { pageSize } from "../../helper/constants";
import ProductItem from "../../components/Product";
import Input from "../../components/Input";
import { debounceDelayTime } from "../../helper/constants";

const Home: React.FC = () => {
  const [productsData, setProductsData] = useState<product[]>([]);
  const [loading, setLoading] = useState(false);
  const [searchName, setSearchName] = useState<string | undefined>();
  const [searchNameDebounce, setSearchNameDebounce] = useState<
    string | undefined
  >(undefined);
  const sentinelRef = useRef<HTMLDivElement | null>(null);

  useEffect(() => {
    // get init products data
    getProductsData();
  }, []);

  // handle debounce when onchange search products
  useEffect(() => {
    const timeoutId = setTimeout(() => {
      setSearchNameDebounce(searchName);
    }, debounceDelayTime);

    return () => clearTimeout(timeoutId);
  }, [searchName]);

  // handle search products by name
  useEffect(() => {
    if (searchNameDebounce !== undefined) {
      handleSearchByName(searchNameDebounce);
    }
  }, [searchNameDebounce]);

  const handleIntersection: IntersectionObserverCallback = (entries) => {
    const target = entries[0];
    if (target.isIntersecting && productsData.length >= pageSize && !loading) {
      getProductsData();
    }
  };

  // handle infinite scroll
  useEffect(() => {
    const options: IntersectionObserverInit = {
      root: null,
      rootMargin: "0px",
      threshold: 1.0,
    };

    const observer = new IntersectionObserver(handleIntersection, options);
    if (sentinelRef.current) {
      observer.observe(sentinelRef.current);
    }

    // Cleanup observer on component unmount
    return () => {
      if (sentinelRef.current) {
        observer.unobserve(sentinelRef.current);
      }
    };
  }, [loading, productsData.length]);

  const handleSearchByName = async (name: string): Promise<void> => {
    if (name) {
      const rs = await getProductsByName(name);
      setProductsData((prev) =>
        rs?.products && rs?.products !== prev ? rs?.products : prev
      );
    } else {
      const rs = await getProducts(pageSize);
      setProductsData((prev) =>
        rs?.products ? [...prev, ...rs?.products] : prev
      );
    }
  };

  // handle get products data
  const getProductsData = async (): Promise<void> => {
    setLoading(true);
    const rs = await getProducts(pageSize);
    setProductsData((prev) =>
      rs?.products ? [...prev, ...rs?.products] : prev
    );
    setLoading(false);
  };

  const renderProducts = (productItem: product) => {
    return <ProductItem data={productItem} />;
  };

  const handleChangeSearch = (value: string) => {
    setSearchName(value);
  };

  return (
    <div className="container" id="home-container">
      <Input
        value={searchName || ""}
        placeholder="Product name..."
        handleOnChange={handleChangeSearch}
      />
      {productsData.length > 0 ? (
        productsData.map(renderProducts)
      ) : (
        <div>No Product Found</div>
      )}
      {productsData.length >= 10 && (
        <div ref={sentinelRef} className="sentinel"></div>
      )}
      {loading && <p>Loading...</p>}
    </div>
  );
};

export default Home;
