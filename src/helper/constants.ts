const loadMoreNum = 20;
const pageSize = 20;
const debounceDelayTime = 500;
export { loadMoreNum, pageSize, debounceDelayTime };
