export const getProducts = async (pageSize: number) =>
  await fetch(
    `https://dummyjson.com/products?limit=${pageSize}&skip=${pageSize}`
  )
    .then((res) => res.json())
    .catch((err) => {
      alert(err);
      return {};
    });

export const getProductsByName = async (name: string) =>
  await fetch(`https://dummyjson.com/products/search?q=${name}`)
    .then((res) => res.json())
    .catch((err) => {
      alert(err);
      return {};
    });
