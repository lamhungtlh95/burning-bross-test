import "./index.css";

interface Props {
  value: string | number;
  placeholder?: string;
  handleOnChange: (value: string) => void;
}

const Input: React.FC<Props> = ({ value, placeholder, handleOnChange }) => {
  const onChange = (event: { target: { value: string } }) => {
    handleOnChange(event.target.value);
  };
  return (
    <div>
      <input placeholder={placeholder} value={value} onChange={onChange} />
    </div>
  );
};
export default Input;
