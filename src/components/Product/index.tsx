import product from "../../modal/product";
import "./index.css";

interface Props {
  data: product;
}

const ProductItem: React.FC<Props> = ({ data }) => {
  const { id, title, description, price, brand } = data;

  return (
    <div key={id} className="product-container">
      <div>
        <span>Name: </span>
        <span>{title || ""}</span>
      </div>
      <div>
        <span>Descriptions: </span>
        <span>{description || ""}</span>
      </div>
      <div>
        <span>Price: </span>
        <span>{price || ""}</span>
      </div>
      <div>
        <span>Brand: </span>
        <span>{brand || ""}</span>
      </div>
    </div>
  );
};
export default ProductItem;
