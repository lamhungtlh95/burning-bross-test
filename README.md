To start project. First, you have to run:
### `npm install`
And then run:
### `npm start`

To view project on browser. After start project success, open browser tab with the link:
### `http://localhost:3000/`

To test the scroll infinite. Please keep scrolling in to the end of page to see how data fetching and update UI.

To test the searching feature. Please enter the name of product you wanna searching:
 - Start call API https://dummyjson.com/products/search?q=phone after 0.5s when user stop typing text in to the search input.
 - When clear input value. Call API 'https://dummyjson.com/products' to get products data
 - If no data fetching, show UI with the text 'No Product found'

Error handling:
 - show alert with API error message return.

